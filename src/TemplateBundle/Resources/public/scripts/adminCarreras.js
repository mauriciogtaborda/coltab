viandaApp.controller('adminCarrerasController', ['$scope', 'adminCarreras', function($scope, adminCarreras) {
jQuery('#container_wait_modal').modal('hide');

  $scope.getData = function(){
    adminCarreras.query({entity:'Carrera'},function(datasLists){
      $scope.datasLists = datasLists;
      jQuery('#container_wait_modal').modal('hide');
    })
  }
  $scope.getData();

  $scope.add = function(form) {
    $scope.dataAdd = {};
    $scope.formAdd.$setPristine();
    $scope.formAdd.$setUntouched();
    jQuery('#addData .modal-header h3').html('Agregar Carrera');
  }

  $scope.edit = function(data) {
    $scope.dataAdd = angular.copy(data);
    jQuery('#addData .modal-header h3').html('Editar Carrera');
  }
  $scope.del = function(data) {
    $scope.dataDel = data;
  }

  $scope.saveData = function(data,form) {
    if(form.$valid){
      jQuery('#addData').modal('hide');
      jQuery('#editData').modal('hide');
      jQuery('#container_wait_modal').modal('show');
      adminCarreras.save({entity:'Carrera'},data,
        function(){
        $scope.getData();
        },
        function(jqXHR){
          errorMsg = jqXHR.data.result;
          jQuery('#warningMessege').modal('show');
          jQuery('#container_wait_modal').modal('hide');
          jQuery('#warningMessege .modal-body p').html(errorMsg);
        }
      )
    }
  }

  $scope.deleteData = function(data) {
    jQuery('#delData').modal('hide');
    jQuery('#container_wait_modal').modal('show');
    adminCarreras.delete({entity:'Carrera',data: data},function(){
      $scope.getData();
    },function(jqXHR){
        errorMsg = jqXHR.data.result;
        jQuery('#container_wait_modal').modal('hide');
        jQuery('#warningMessege').modal('show');
        jQuery('#warningMessege .modal-body p').html(errorMsg);        
    })
  }

}])


