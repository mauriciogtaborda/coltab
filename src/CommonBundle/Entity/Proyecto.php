<?php

namespace CommonBundle\Entity;

use JMS\Serializer\Annotation\Groups;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Proyecto
 * 
 * @ORM\Table()
 * @ORM\Entity
 */
class Proyecto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"proyecto"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=50)
     * @Groups({"proyecto"})
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=150)
     * @Groups({"proyecto"})
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="problematica", type="string", length=150)
     * @Groups({"proyecto"})
     */
    private $problematica;

    /**
     * @var string
     *
     * @ORM\Column(name="organizacion", type="string", length=150)
     * @Groups({"proyecto"})
     */
    private $organizacion;


    /**
     * @var string
     *
     * @ORM\Column(name="objetivo", type="string", length=150)
     * @Groups({"proyecto"})
     */
    private $objetivo;

    /**
     * @var string
     *
     * @ORM\Column(name="alcance", type="string", length=150)
     * @Groups({"proyecto"})
     */
    private $alcance;

     /**
     * @var integer
     *
     * @ORM\OneToOne(targetEntity="TipoProyecto", inversedBy="proyectos")     
     * @ORM\JoinColumn(name="tipoProyectoId", referencedColumnName="id")
     * @Groups({"tipoProyecto"})
     */
    private $tipoProyectos;
    
     /**
     * @var integer
     *
     * @ORM\OneToOne(targetEntity="Metodologia", inversedBy="proyectos")     
     * @ORM\JoinColumn(name="metodologiaId", referencedColumnName="id")
     * @Groups({"tipoProyecto"})
     */
    private $metodologias;
     
     /**
     * @var string
     *
     * @ORM\Column(name="planificacion", type="string", length=150)
     * @Groups({"proyecto"})
     */
    private $planificacion;
    
     /**
     * @var string
     *
     * @ORM\Column(name="hardware", type="string", length=150)
     * @Groups({"proyecto"})
     */
    private $hardware;
    
    
     /**
     * @var string
     *
     * @ORM\Column(name="lenguaje", type="string", length=150)
     * @Groups({"proyecto"})
     */
    private $lenguaje;
    
    
     /**
     * @var string
     *
     * @ORM\Column(name="apis", type="string", length=150)
     * @Groups({"proyecto"})
     */
    private $apis;

     /**
     * @var string
     *
     * @ORM\Column(name="reqFuncionales", type="string", length=150)
     * @Groups({"proyecto"})
     */
    private $reqFuncionales;
    
     /**
     * @var string
     *
     * @ORM\Column(name="reqNoFuncionales", type="string", length=150)
     * @Groups({"proyecto"})
     */
    private $reqNoFuncionales;
    
    /**
     * @ORM\ManyToOne(targetEntity="Curso", inversedBy="proyectos")
     * @ORM\JoinColumn(name="curso_id", referencedColumnName="id")
     * @Groups({"proyecto"})
     */
    protected $cursos;
    
   

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Proyecto
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Proyecto
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set problematica
     *
     * @param string $problematica
     * @return Proyecto
     */
    public function setProblematica($problematica)
    {
        $this->problematica = $problematica;

        return $this;
    }

    /**
     * Get problematica
     *
     * @return string 
     */
    public function getProblematica()
    {
        return $this->problematica;
    }

    /**
     * Set organizacion
     *
     * @param string $organizacion
     * @return Proyecto
     */
    public function setOrganizacion($organizacion)
    {
        $this->organizacion = $organizacion;

        return $this;
    }

    /**
     * Get organizacion
     *
     * @return string 
     */
    public function getOrganizacion()
    {
        return $this->organizacion;
    }

    /**
     * Set objetivo
     *
     * @param string $objetivo
     * @return Proyecto
     */
    public function setObjetivo($objetivo)
    {
        $this->objetivo = $objetivo;

        return $this;
    }

    /**
     * Get objetivo
     *
     * @return string 
     */
    public function getObjetivo()
    {
        return $this->objetivo;
    }

    /**
     * Set alcance
     *
     * @param string $alcance
     * @return Proyecto
     */
    public function setAlcance($alcance)
    {
        $this->alcance = $alcance;

        return $this;
    }

    /**
     * Get alcance
     *
     * @return string 
     */
    public function getAlcance()
    {
        return $this->alcance;
    }

    /**
     * Set planificacion
     *
     * @param string $planificacion
     * @return Proyecto
     */
    public function setPlanificacion($planificacion)
    {
        $this->planificacion = $planificacion;

        return $this;
    }

    /**
     * Get planificacion
     *
     * @return string 
     */
    public function getPlanificacion()
    {
        return $this->planificacion;
    }

    /**
     * Set hardware
     *
     * @param string $hardware
     * @return Proyecto
     */
    public function setHardware($hardware)
    {
        $this->hardware = $hardware;

        return $this;
    }

    /**
     * Get hardware
     *
     * @return string 
     */
    public function getHardware()
    {
        return $this->hardware;
    }

    /**
     * Set lenguaje
     *
     * @param string $lenguaje
     * @return Proyecto
     */
    public function setLenguaje($lenguaje)
    {
        $this->lenguaje = $lenguaje;

        return $this;
    }

    /**
     * Get lenguaje
     *
     * @return string 
     */
    public function getLenguaje()
    {
        return $this->lenguaje;
    }

    /**
     * Set apis
     *
     * @param string $apis
     * @return Proyecto
     */
    public function setApis($apis)
    {
        $this->apis = $apis;

        return $this;
    }

    /**
     * Get apis
     *
     * @return string 
     */
    public function getApis()
    {
        return $this->apis;
    }

    /**
     * Set reqFuncionales
     *
     * @param string $reqFuncionales
     * @return Proyecto
     */
    public function setReqFuncionales($reqFuncionales)
    {
        $this->reqFuncionales = $reqFuncionales;

        return $this;
    }

    /**
     * Get reqFuncionales
     *
     * @return string 
     */
    public function getReqFuncionales()
    {
        return $this->reqFuncionales;
    }

    /**
     * Set reqNoFuncionales
     *
     * @param string $reqNoFuncionales
     * @return Proyecto
     */
    public function setReqNoFuncionales($reqNoFuncionales)
    {
        $this->reqNoFuncionales = $reqNoFuncionales;

        return $this;
    }

    /**
     * Get reqNoFuncionales
     *
     * @return string 
     */
    public function getReqNoFuncionales()
    {
        return $this->reqNoFuncionales;
    }

    /**
     * Set tipoProyecto
     *
     * @param \CommonBundle\Entity\TipoProyecto $tipoProyecto
     * @return Proyecto
     */
    public function setTipoProyecto(\CommonBundle\Entity\TipoProyecto $tipoProyecto = null)
    {
        $this->tipoProyecto = $tipoProyecto;

        return $this;
    }

    /**
     * Get tipoProyecto
     *
     * @return \CommonBundle\Entity\TipoProyecto 
     */
    public function getTipoProyecto()
    {
        return $this->tipoProyecto;
    }

    /**
     * Set metodologia
     *
     * @param \CommonBundle\Entity\Metodologia $metodologia
     * @return Proyecto
     */
    public function setMetodologia(\CommonBundle\Entity\Metodologia $metodologia = null)
    {
        $this->metodologia = $metodologia;

        return $this;
    }

    /**
     * Get metodologia
     *
     * @return \CommonBundle\Entity\Metodologia 
     */
    public function getMetodologia()
    {
        return $this->metodologia;
    }
}
