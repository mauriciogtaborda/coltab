<?php

namespace CommonBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use CommonBundle\Entity\Paciente;
use CommonBundle\Entity\Efector;
use CommonBundle\Entity\User;
use CommonBundle\Entity\Carrera;

use Symfony\Component\Debug\Debug;
Debug::enable();


class APIAdminController extends Controller
{
 ######   ######## ######## 
##    ##  ##          ##    
##        ##          ##    
##   #### ######      ##    
##    ##  ##          ##    
##    ##  ##          ##    
 ######   ########    ##    

    public function getDataAction($em, $reference, $getData){
        try{
            switch ($reference){
                case 'Efector':
                    $query = $em->createQuery('
                            SELECT r FROM CommonBundle:Regimen r
                            INNER JOIN r.efector ef 
                            WHERE ef.id = :efectorId');
                    $query->setParameter('efectorId',$getData);
                    return $query;
                    break;

            }

        }catch (\Exception $e) {
            $jsonContent = json_encode($e->getMessage());
            return $jsonContent;
        }
    }

   ###    ########  ########        ##    ##       ######## ########  #### ######## 
  ## ##   ##     ## ##     ##        ##  ##        ##       ##     ##  ##     ##    
 ##   ##  ##     ## ##     ##         ####         ##       ##     ##  ##     ##    
##     ## ##     ## ##     ##          ##          ######   ##     ##  ##     ##    
######### ##     ## ##     ##          ##          ##       ##     ##  ##     ##    
##     ## ##     ## ##     ##          ##          ##       ##     ##  ##     ##    
##     ## ########  ########           ##          ######## ########  ####    ##      
    public function addANDeditAllDataAction(){
        try{
            $entity = $this->getRequest()->query->get('ent');

            $em = $this->container->get('doctrine')->getManager();
            $postContent = $this->get('request')->getContent();
            $json_object = json_decode($postContent, true);


            //SEGURIDAD 
            if(false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
                return new Response("{\"result\":\"Acceso Denegado\"}", 401);
                //throw new \Exception('Acceso Denegado');
            }

            //array('ENTIDAD','URL')
            $user = $this->container->get('security.context')->getToken()->getUser(); 
            $rolesUser = $user->getRoles();
            $rolUser = $rolesUser[0];

            $route = $this->getRequest()->attributes->get('_route');
            
            $RESTValidation = new RESTValidation();
            $checkApproved = $RESTValidation->entityANDurlApproved($entity, $route, $rolesUser);
            if(!$checkApproved){
                throw new \Exception("Entidad no Aprobada");
            }


            $postSave = true;
            switch($entity){

                case 'Efector':
                    $tipoEfector = $em->getRepository("CommonBundle:TipoEfector")->findOneById($json_object['tipo_efector']['id']);
                    if(isset($json_object['id'])){
                        $id = $json_object['id'];
                        $saveData = $em->getRepository("CommonBundle:Efector")->findOneById($id);

                        //busco Efector duplicado
                        $filter = array('descripcion'=>$json_object['descripcion'], 'fechaBaja'=>null);
                        $efectores = $em->getRepository("CommonBundle:Efector")->findBy($filter);
                        if(count($efectores) != 0){
                            if($json_object['descripcion'] != $saveData->getDescripcion()){
                                throw new \Exception('Ya existe un Efector con esa descripción');
                            }
                        }
                        //busco Efector duplicado por ID Sigehos
                        $filter = array('sigehos'=>$json_object['sigehos'], 'fechaBaja'=>null);
                        $efectores = $em->getRepository("CommonBundle:Efector")->findBy($filter);
                        if(count($efectores) != 0){
                            if($json_object['sigehos'] != $saveData->getSigehos()){
                                throw new \Exception('Ya existe un Efector con ese ID de Sigehos');
                            }
                        }  
                    }else{
                        $saveData = new Efector();

                        //busco Efector duplicado
                        $filter = array('descripcion'=>$json_object['descripcion'], 'fechaBaja'=>null);
                        $efectores = $em->getRepository("CommonBundle:Efector")->findBy($filter); 
                        if(count($efectores) != 0){
                            throw new \Exception('Ya existe un Efector con esa descripción');
                        }
                        //busco Efector duplicado por ID Sigehos
                        $filter = array('sigehos'=>$json_object['sigehos'], 'fechaBaja'=>null);
                        $efectores = $em->getRepository("CommonBundle:Efector")->findBy($filter); 
                        if(count($efectores) != 0){
                            throw new \Exception('Ya existe un Efector con ese ID de Sigehos');
                        }
                    }
                    $saveData->setDescripcion($json_object['descripcion']);          
                    $saveData->setNovedades(false);          
                    $saveData->setTipoEfector($tipoEfector);      
                    $saveData->setSigehos($json_object['sigehos']);      

                    break;

               
             
                case 'Paciente':
                    $unidad = $em->getRepository("CommonBundle:UnidadInternacion")->findOneById($json_object['unidad_internacion']['id']);
                    $genero = $em->getRepository("CommonBundle:Genero")->findOneById($json_object['genero']['id']);
                    $tipoDoc = $em->getRepository("CommonBundle:TipoDocumento")->findOneById($json_object['tipo_documento']['id']);
                    $json_object['docnro'] = preg_replace('([^A-Za-z0-9])', '', $json_object['docnro']);
                    if(isset($json_object['id'])){
                        $id = $json_object['id'];
                        $saveData = $em->getRepository("CommonBundle:Paciente")->findOneById($id);

                        //busco Paciente duplicado
                        $filter = array('docnro'=>ltrim($json_object['docnro'],0),
                                        'tipoDocumento'=>$json_object['tipo_documento']['id'], 
                                        'fechaBaja'=>null);
                        $nrodocumento = $em->getRepository("CommonBundle:Paciente")->findBy($filter);
                        if(count($nrodocumento) != 0){
                            if(ltrim($json_object['docnro'],0) != $saveData->getDocnro() ||
                               $json_object['tipo_documento']['id'] != $saveData->getTipoDocumento()->getId()){
                                throw new \Exception('Ya existe un Paciente con ese tipo y número de documento');
                            }
                        } 
                    }else{
                        $saveData = new Paciente();

                        //busco Paciente duplicado
                        $filter = array('docnro'=>ltrim($json_object['docnro'],0),
                                        'tipoDocumento'=>$json_object['tipo_documento']['id'], 
                                        'fechaBaja'=>null);
                        $nrodocumento = $em->getRepository("CommonBundle:Paciente")->findBy($filter); 
                        if(count($nrodocumento) != 0){
                            throw new \Exception('Ya existe un Paciente con ese tipo y número de documento');
                        }
                    }
                    $date = \DateTime::createFromFormat('d/m/Y',$json_object['fecha_nacimiento']);
                    $fechaHoy = new \DateTime();
                    if($date > $fechaHoy){
                        throw new \Exception('La fecha de nacimiento no puede ser mayor a hoy');
                    }

                    $json_object['apellido'] = preg_replace('([^a-zA-ZñÑáéíóúÁÉÍÓÚÜüöÖäÄ\s])', '', $json_object['apellido']);
                    $saveData->setApellido($json_object['apellido']);
                    $json_object['nombre'] = preg_replace('([^a-zA-ZñÑáéíóúÁÉÍÓÚÜüöÖäÄ\s])', '', $json_object['nombre']);
                    $saveData->setNombre($json_object['nombre']);
                    $saveData->setTipoDocumento($tipoDoc);             
                    $saveData->setDocnro(ltrim($json_object['docnro'],0));
                    $saveData->setGenero($genero);
                    $saveData->setIsNotSigheos(true);
                    $saveData->setUnidadInternacion($unidad);
                    (isset($json_object['observaciones'])) ? $saveData->setObservaciones($json_object['observaciones']):0;     
                    $saveData->setFechaNacimiento($date);
                    (isset($json_object['cama'])) ? $saveData->setCama(ltrim(preg_replace('([^A-Za-z0-9])', '', $json_object['cama']),0)) : 0;                                           

                    break;

                case 'Carrera':
                    if(isset($json_object['id'])){
                        $id = $json_object['id'];
                        $saveData = $em->getRepository("CommonBundle:Carrera")->findOneById($id); 
                    }else{
                        $saveData = new Carrera();
                    }
                    //busco Carrera duplicado
                    $filter = array('nombre'=>$json_object['nombre'], 'fechaBaja'=>null);
                    $carrera = $em->getRepository("CommonBundle:Carrera")->findBy($filter); 
                    if(count($carrera) != 0){
                        throw new \Exception('Ya existe una Carrera con ese nombre');
                    }

                    $saveData->setNombre($json_object['nombre']);
                    break;

               
                case 'User':                   
                    $caracteres = 'ABCDEFGHJKLMNPQRSTUVWXYZabcdefghjkmnpqrstuvwxyz123456789';
                    $catidadCaracteres = 6;
                    $randomGenerate = '';
                    for ($i=0; $i < $catidadCaracteres; $i++){ 
                        $desordenada = str_shuffle($caracteres);
                        $random = substr($desordenada, 1, 1);
                        $randomGenerate .= $random;
                    }
                    if(isset($json_object['id'])){
                        $id = $json_object['id'];
                        $saveData = $em->getRepository("CommonBundle:User")->findOneById($id);

                        //valido username
                        $user = $em->getRepository("CommonBundle:User")->findOneByUsername($json_object['username']);
                        if(count($user) != 0){
                            if($json_object['username'] != $saveData->getUsername()){
                                throw new \Exception("Ya existe un Usuario con ese Email");
                            }
                        }
                        //valido documento
                        $user = $em->getRepository("CommonBundle:User")->findOneByDocnro($json_object['docnro']);
                        if(count($user) != 0){
                            if($json_object['docnro'] != $saveData->getDocnro()){
                                throw new \Exception("Ya existe un Usuario con ese Documento");
                            }
                        }  

                        //valido si es un blanqueo start
                        if(isset($json_object['blanqueo'])){
                            $saveData->setConfirmationToken(md5($randomGenerate));
                            $saveData->setPin($randomGenerate);
                            $saveData->setEnabled(false);
                        }
                        //valido si es un blanqueo end
                    }else{
                        //valido username
                        $user = $em->getRepository("CommonBundle:User")->findOneByUsername($json_object['username']);
                        if(count($user) != 0){
                            throw new \Exception("Ya existe un Usuario con ese Email");
                        }
                        //valido documento
                        $user = $em->getRepository("CommonBundle:User")->findOneByDocnro($json_object['docnro']);
                        if(count($user) != 0){
                            throw new \Exception("Ya existe un Usuario con ese Documento");
                        }

                        $saveData = new User();
                        $saveData->setPlainPassword($randomGenerate);
                        $saveData->setConfirmationToken(md5($randomGenerate));
                        $saveData->setPin($randomGenerate);
                        $saveData->setUsername($json_object['username']);
                        $saveData->setEmail($json_object['username']);
                        $saveData->setRoles($json_object['roles']);
                    }

                    if(preg_match('([^a-zA-ZñÑáéíóúÁÉÍÓÚÜüöÖäÄ\s])', $json_object['nombre'])){ throw new \Exception("Nombre inválido"); }
                    if($json_object['nombre']==''){ throw new \Exception("Nombre inválido"); }
                    $saveData->setNombre($json_object['nombre']);

                    if(preg_match('([^a-zA-ZñÑáéíóúÁÉÍÓÚÜüöÖäÄ\s])', $json_object['apellido'])){ throw new \Exception("Apellido inválido"); }
                    if($json_object['apellido']==''){ throw new \Exception("Apellido inválido"); }
                    $saveData->setApellido($json_object['apellido']);

                    if(preg_match('([^0-9])', $json_object['docnro'])){ throw new \Exception("Documento inválido"); }
                    if($json_object['docnro']==''){ throw new \Exception("Documento inválido"); }
                    $saveData->setDocnro($json_object['docnro']);
                    
                    if(isset($json_object['efector']['id'])){
                        $efector = $em->getRepository("CommonBundle:Efector")->findOneById($json_object['efector']['id']);
                        $saveData->setEfector($efector);
                    }
                    
                    //envio mail para activar cuenta
                    if(!isset($json_object['id']) || isset($json_object['blanqueo'])){
                        $subject = 'Bienvenido al Sistema de Gestión de Raciones';        
                        $remitente = array($this->container->getParameter('mailer_user')=>'Sistema Viandas');        
                        $destinatario = $json_object['username'];
                        $templateName = 'CommonBundle:Email:sendPassword.html.twig';
                        $urlApi = $this->container->getParameter('url_api');
                        $message = \Swift_Message::newInstance();
                        $params = array(
                                        'clave'=>$randomGenerate,
                                        'urlValidUser'=>$urlApi.'validateAccount?account='.$json_object['username'].'&token='.md5($randomGenerate),
                                        'img'=>$message->embed(\Swift_Image::fromPath('../web/images/ba-btn.png'))
                                        );
                        $EMAIL = new EMAIL($this->container);
                        $EMAIL->sendMailer($message, $subject, $remitente, $destinatario, $templateName, $params);  

                        $em->persist($saveData);
                        $em->flush();

                        if(isset($json_object['blanqueo'])){
                            return new Response("{\"result\":\"Se ha blanqueado correctamente la contraseña de </br> Username: ".$json_object['username']."\"}", 200);
                        }    
                    }else{
                        //guardo nuevo usuario
                        $em->persist($saveData);
                        $em->flush();                        
                    }

                    $postSave = false;

                    break;

                // case 'Racion':
                //     if(isset($json_object['asistentes'])){
                //         //MODULO ESPECIAL
                //         $fecha = \DateTime::createFromFormat('d/m/Y',$json_object['fecha']['fechaevento']);
                //         if(isset($json_object['asistentes'][0]['racion']['id'])){
                //             $id = $json_object['asistentes'][0]['racion']['id'];
                //             $racion = $em->getRepository("CommonBundle:Racion")->findOneById($id);
                //         }else{
                //             $racion = new Racion();
                //         }

                //         $racion->setIsCrudo($json_object['is_crudo']);     
                //         $racion->setFecha($fecha);
                //         $tipoComida = $em->getRepository("CommonBundle:TipoComida")->findOneById($json_object['modulos_pacientes']['tipocomida']['id']);
                //         $racion->setTipoComida($tipoComida);
                //         $regimen = $em->getRepository("CommonBundle:Regimen")->findOneById($json_object['regimen']['id']);
                //         $estado = $em->getRepository("CommonBundle:Estado")->findOneById(1);
                //         $racion->setEstado($estado);
                //         $racion->setRegimen($regimen);                
                //         if(isset($json_object['asistentes'][0]['paciente'])){
                //             $racion->setComensales(count($json_object['asistentes']));   
                //             $em->persist($racion);             
                //             $eliminoRacion = false;
                //         }else{
                //             $eliminoRacion = true;
                //         }
                        
                //         $em->flush();

                //         //ELIMINO ASISTENTES
                //         if(isset($json_object['asistentes'][0]['racion']['id']) && isset($json_object['asistentes'][0]['paciente'])){
                //             $filter = array('racion'=>$id,'modulosPacientesDetalles'=>$json_object['id']);
                //             $racionModulo = $em->getRepository("CommonBundle:RacionModuloPaciente")->findBy($filter);
                //             foreach ($racionModulo as $key => $valueRacionModulo) {
                //                 foreach ($json_object['asistentes'] as $asistentes) {
                //                     if(count($racionModulo) != 0){
                //                         if($valueRacionModulo->getPaciente()->getId() == $asistentes['paciente']['id']){
                //                             unset($key);
                //                         }
                //                     }
                //                 }
                //             }                        
                //         }else{
                //             $filter = array('modulosPacientesDetalles'=>$json_object['id']);
                //             $deleteAll = $em->getRepository("CommonBundle:RacionModuloPaciente")->findBy($filter);
                //         }
                        

                //         //AGREGO LOS NUEVOS ASISTENTES
                //         if(isset($racionModulo)){
                //             foreach ($json_object['asistentes'] as $key => $asistentes) {
                //                 foreach ($racionModulo as $valueRacionModulo) {
                //                     if(count($racionModulo) != 0){
                //                         if($valueRacionModulo->getPaciente()->getId() == $asistentes['paciente']['id']){
                //                             unset($key);
                //                         }                                
                //                     }
                //                 }
                //             }
                //         }

                //         if(isset($json_object['asistentes'][0]['racion']['id'])){
                //             $idRacion = $json_object['asistentes'][0]['racion']['id'];
                //         }else{
                //             $idRacion = $racion->getId();
                //         }

                //         if(isset($json_object['asistentes'][0]['paciente'])){
                //             foreach ($json_object['asistentes'] as $value) {
                //                 $RacionModulo = new RacionModuloPaciente();
                //                 $paciente = $em->getRepository("CommonBundle:Paciente")->findOneById($value['paciente']['id']);           
                //                 $RacionModulo->setPaciente($paciente);
                //                 $racion = $em->getRepository("CommonBundle:Racion")->findOneById($idRacion);           
                //                 $RacionModulo->setRacion($racion);
                //                 $moduloPaciente = $em->getRepository("CommonBundle:ModuloPacienteDetalle")->findOneById($json_object['id']);
                //                 $RacionModulo->setModulosPacientesDetalles($moduloPaciente);
                //                 $em->persist($RacionModulo);
                //             }
                //         }
                //         if(isset($racionModulo)){
                //             foreach ($racionModulo as $racionMod){
                //                 if(isset($json_object['asistentes'][0]['racion']['id'])){
                //                     $filter = array('paciente'=>$racionMod->getPaciente()->getId(),'racion'=>$id,'modulosPacientesDetalles'=>$json_object['id']);
                //                 }else{
                //                     $filter = array('paciente'=>$racionMod->getPaciente()->getId(),'racion'=>$racion->getId(),'modulosPacientesDetalles'=>$json_object['id']);
                //                 }
                //                 $racionModulo = $em->getRepository("CommonBundle:RacionModuloPaciente")->findBy($filter);  
                //                 $em->remove($racionModulo[0]);
                //             }
                //         }

                //         if(isset($deleteAll)){
                //             foreach ($deleteAll as $racionMod){
                //                 $em->remove($racionMod);
                //             }
                //         }

                //         if($eliminoRacion){
                //             $em->remove($racion);
                //         }

                //         $em->flush();
                //         $postSave = false;

                //     }elseif(isset($json_object['estado'])){
                //         //CONTROL DE RACIONES
                //         if(isset($json_object['raciones'])){
                //             foreach ($json_object['raciones'] as $key => $value) {
                //                 if($value != null){
                //                     $saveData = $em->getRepository("CommonBundle:Racion")->findOneById($value['id']);     
                //                     if(isset($value['observaciones'])){
                //                         $saveData->setObservaciones($value['observaciones']);
                //                     }    
                //                     switch ($rolUser) {
                //                         case 'ROLE_CONCESIONARIO':
                //                             $estado = $em->getRepository("CommonBundle:Estado")->findOneById(2);
                //                             break;

                //                         case 'ROLE_NUTRICIONISTA':
                //                         case 'ROLE_JEFESERVICIO':
                //                             $estado = $em->getRepository("CommonBundle:Estado")->findOneById(3);
                //                             break;
                //                     }
                //                     $saveData->setEstado($estado);
                //                     $em->persist($saveData);
                //                 }
                //             }
                //             $em->flush();
                //             $postSave = false;
                //         }
                //     }else{
                //         //GESTION REGIMEN ALIMENTACION E INFANTES
                //         $paciente = $em->getRepository("CommonBundle:Paciente")->findOneById($json_object['id']);           
                //         $tipoComida = $em->getRepository("CommonBundle:TipoComida")->findOneById($json_object['tipoComida']['idTipoComida']);
                //         $estado = $em->getRepository("CommonBundle:Estado")->findOneById(1);

                //         $efectorId = $paciente->getUnidadInternacion()->getEfector()->getId();
                //         $query = $em->createQuery('
                //                 SELECT r FROM CommonBundle:Regimen r
                //                 INNER JOIN r.efector ef where ef.id = :efectorId');
                //         $query->setParameter('efectorId',$efectorId);
                //         $data = $query->getResult();
                //         if(count($data) == 0){
                //             throw new \Exception("No existen regimenes para este efector");
                //         }

                //         if(isset($json_object['racion']['id'])){
                //             $id = $json_object['racion']['id'];
                //             $saveData = $em->getRepository("CommonBundle:Racion")->findOneById($id);            
                //         }else{
                //             $saveData = new Racion();
                //         }
                //         $date = \DateTime::createFromFormat('d/m/Y',$json_object['racion']['fecha']);
                //         $saveData->setFecha($date);
                //         $saveData->setIsCrudo($json_object['racion']['is_crudo']);
                //         $saveData->setIsSinSal($json_object['racion']['is_sinsal']);
                //         $saveData->setIsCortado($json_object['racion']['is_cortado']);
                //         $saveData->setIsAcompanante($json_object['racion']['is_acompanante']);
                //         $saveData->setIsPicado($json_object['racion']['is_picado']);
                //         $saveData->setIsLicuado($json_object['racion']['is_licuado']);
                //         $saveData->setIsMenor($json_object['racion']['is_menor']);
                //         $saveData->setIsDescartable($json_object['racion']['is_descartable']);
                //         $saveData->setIsDobleRacion($json_object['racion']['is_doble_racion']);
                //         $saveData->setEstado($estado);

                //         $comensales = 1;
                //         ($json_object['racion']['is_doble_racion']) ? $comensales++ : 0;
                //         ($json_object['racion']['is_acompanante']) ? $comensales++ : 0;
                //         $saveData->setComensales($comensales);

                //         if(isset($json_object['racion']['observaciones'])){
                //             $saveData->setObservaciones($json_object['racion']['observaciones']);
                //         }           
                //         if(isset($json_object['regimen']['id'])){
                //             $regimen = $em->getRepository("CommonBundle:Regimen")->findOneById($json_object['regimen']['id']);
                //             $saveData->setRegimen($regimen);                
                //         }
                //         $saveData->setPaciente($paciente);
                //         $saveData->setTipoComida($tipoComida);

                //         //borro todos los refuerzos para que no haya clave duplicada
                //         if(isset($json_object['grupoRefuerzos'])){
                //             foreach ($json_object['grupoRefuerzos'] as $key) {
                //                 foreach ($key['refuerzos'] as $ref) {
                //                     $refuerzo = $em->getRepository("CommonBundle:Refuerzo")->findOneById($ref['id']);
                //                     $saveData->removeRefuerzo($refuerzo);
                //                 }
                //             } 
                //         }                    

                //         //agrego los refuerzos
                //         if(isset($json_object['grupoRefuerzos'])){
                //             foreach ($json_object['grupoRefuerzos'] as $key) {
                //                 foreach ($key['refuerzos'] as $ref) {
                //                     $refuerzo = $em->getRepository("CommonBundle:Refuerzo")->findOneById($ref['id']);
                //                     if(isset($ref['check'])){
                //                         if($ref['check']){
                //                             $saveData->addRefuerzo($refuerzo);
                //                         }
                //                     }
                //                 }
                //             } 
                //         }
                //     }    
                //     break;

            }
            if($postSave){
                $em->persist($saveData);
                $em->flush();
            }

            return new Response("{\"result\":\"".$entity." Saved.\"}", 200);
        }catch (\Exception $e) {

            return new Response("{\"result\":\"".$e->getMessage()."\"}", 500);
        }
    }



########  ######## ##       ######## ######## ######## 
##     ## ##       ##       ##          ##    ##       
##     ## ##       ##       ##          ##    ##       
##     ## ######   ##       ######      ##    ######   
##     ## ##       ##       ##          ##    ##       
##     ## ##       ##       ##          ##    ##       
########  ######## ######## ########    ##    ######## 
    public function deleteAllDataAction(){
        try{
            $entity = $this->getRequest()->query->get('ent');
            $em = $this->container->get('doctrine')->getManager();


            //SEGURIDAD 
            if(false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
                return new Response("{\"result\":\"Acceso Denegado\"}", 401);
                //throw new \Exception('Acceso Denegado');
            }

            //array('ENTIDAD','URL')
            $user = $this->container->get('security.context')->getToken()->getUser(); 
            $rolesUser = $user->getRoles();
            $rolUser = $rolesUser[0];

            $route = $this->getRequest()->attributes->get('_route');
            
            $RESTValidation = new RESTValidation();
            $checkApproved = $RESTValidation->entityANDurlApproved($entity, $route, $rolesUser);
            if(!$checkApproved){
                throw new \Exception("Entidad no Aprobada");
            }



            $data = json_decode($this->getRequest()->query->get('data'), true);
            $delData = $em->getRepository("CommonBundle:".$entity)->findOneById($data['id']);
            $delete = false;
            switch($entity){
                
                case 'Carrera':
                       $delData->setFechaBaja(new \DateTime());
                    
                    break;
                case 'Efector':
                        $filterFN = array('efector'=>$data['id'],'fechaBaja'=>null);
                        $filterSF = array('efector'=>$data['id']);
                        $response = "No es posible eliminar el Efector ya que existe una relación con: ";
                        $return = true;

                        $modulo = $em->getRepository("CommonBundle:ModuloPaciente")->findBy($filterFN); 
                        if(count($modulo) != 0){ $return = true; $response .= ' Tipo Módulo Paciente,'; }

                        $moduloEspecial = $em->getRepository("CommonBundle:ModuloEspecial")->findBy($filterSF); 
                        if(count($moduloEspecial) != 0){ $return = true; $response .= ' Módulo Especial,';  }
                        
                        $formula = $em->getRepository("CommonBundle:Formula")->findBy($filterFN); 
                        if(count($formula) != 0){ $return = true; $response .= ' Fórmula,';  }
                        
                        $gruporef = $em->getRepository("CommonBundle:GrupoRefuerzo")->findBy($filterFN); 
                        if(count($gruporef) != 0){ $return = true; $response .= ' Grupo Refuerzo,';  }
                        
                        $stock = $em->getRepository("CommonBundle:Stock")->findBy($filterSF); 
                        if(count($stock) != 0){ $return = true; $response .= ' Stock,';  }
                        
                        $query = $em->createQuery('
                            SELECT r FROM CommonBundle:Regimen r
                            INNER JOIN r.efector ef 
                            WHERE ef.id = :efectorId');
                        $query->setParameter('efectorId',$data['id']);
                        $regimen = $query->getResult();
                        if(count($regimen) != 0){ $return = true; $response .= ' Régimen,';  }
                        
                        $insumo = $em->getRepository("CommonBundle:Insumo")->findBy($filterFN); 
                        if(count($insumo) != 0){ $return = true; $response .= ' Insumo,';  }
                        
                        $especialidad = $em->getRepository("CommonBundle:Especialidad")->findBy($filterFN); 
                        if(count($especialidad) != 0){ $return = true; $response .= ' Especialidad,';  }
                        
                        $servicio = $em->getRepository("CommonBundle:Servicio")->findBy($filterFN); 
                        if(count($servicio) != 0){ $return = true; $response .= ' Servicio,';  }
                        
                        $unidad = $em->getRepository("CommonBundle:UnidadInternacion")->findBy($filterFN); 
                        if(count($unidad) != 0){ $return = true; $response .= ' Unidad de Internación,';  }
                        
                        $personal = $em->getRepository("CommonBundle:Personal")->findBy($filterFN); 
                        if(count($personal) != 0){ $return = true; $response .= ' Personal,';  }
                                                
                        $autorizadoE = $em->getRepository("CommonBundle:AutorizadoEventual")->findBy($filterFN); 
                        if(count($autorizadoE) != 0){ $return = true; $response .= ' Autorizado Eventual,';  }
                        
                        $moduloPaciente = $em->getRepository("CommonBundle:ModuloPacienteDetalle")->findBy($filterSF); 
                        if(count($moduloPaciente) != 0){ $return = true; $response .= ' Módulo Paciente,';  }
                     
                        $efectorConcesionarioTP = $em->getRepository("CommonBundle:EfectorConcesionarioTipoComida")->findBy($filterSF); 
                        if(count($efectorConcesionarioTP) != 0){ $return = true; $response .= ' Concesionario,';  }
                        

                        if($return){ $response = substr($response, 0, -1); $response .= '.'; throw new \Exception($response); }
                        $delData->setFechaBaja(new \DateTime());
                        break;
     
            }
                
            if($delete){
                $em->remove($delData);
            }else{
                $em->persist($delData);
            }
            
            $em->flush();


            return new Response("{\"result\":\"".$entity." Deleted.\"}", 200);
        }catch (\Exception $e) {
            return new Response("{\"result\":\"".$e->getMessage()."\"}", 500);
        }
    }

##        #######   ######   
##       ##     ## ##    ##  
##       ##     ## ##        
##       ##     ## ##   #### 
##       ##     ## ##    ##  
##       ##     ## ##    ##  
########  #######   ######   
    private function logMessage($message, $error = false){
        if($error){
            $this->container->get('logger')->error($message);
        }else{
            $this->container->get('logger')->info($message);
        }
    }


}

