<?php

namespace CommonBundle\Controller;

use Symfony\Component\Debug\Debug;
Debug::enable();


class RESTValidation 
{
    public function entityANDurlApproved($entity, $route, $rolesUser){
        //array(ENTIDAD, ROUTE, ROL)
        $entityApproved = array(
                            
                            'Efector'=>array(
                                            'common_adminEfectores_getAll'=>array('ROLE_ADMIN','ROLE_NUTRICIONISTA','ROLE_JEFESERVICIO','ROLE_REPORTES','ROLE_HEMOTERAPIA','ROLE_JEFATURA','ROLE_CONCESIONARIO','ROLE_RRHH'),
                                            'common_adminEfectores_addANDeditData'=>array('ROLE_ADMIN'),
                                            'common_adminEfectores_deleteData'=>array('ROLE_ADMIN')
                                            ),
                            'Carrera'=>array(
                                            'common_adminCarreras_getAll'=>array('ROLE_ADMIN'),
                                            'common_adminCarreras_addANDeditData'=>array('ROLE_ADMIN'),
                                            'common_adminCarreras_deleteData'=>array('ROLE_ADMIN')
                                            ),
                            'Paciente'=>array(
                                            'common_adminPacientes_getAll'=>array('ROLE_ADMIN','ROLE_NUTRICIONISTA','ROLE_JEFESERVICIO'),
                                            'common_adminPacientes_addANDeditData'=>array('ROLE_ADMIN','ROLE_NUTRICIONISTA','ROLE_JEFESERVICIO'),
                                            'common_adminPacientes_deleteData'=>array('ROLE_ADMIN'),
                                            'common_updateInSigehos'=>array('ROLE_ADMIN')
                                            ),
                            
                            'CalendarioComida'=>array( 
                                            'common_gestionCalendarioComida_getAll'=>array('ROLE_ADMIN'),
                                            'common_gestionCalendarioComida_addANDeditData'=>array('ROLE_ADMIN'),
                                            'common_gestionCalendarioComida_deleteData'=>array('ROLE_ADMIN')
                                            ),
                            'User'=>array(
                                            'common_gestionUsuarios_getAll'=>array('ROLE_ADMIN'),
                                            'common_gestionUsuarios_addANDeditData'=>array('ROLE_ADMIN'),
                                            'common_gestionUsuarios_deleteData'=>array('ROLE_ADMIN')
                                            ),
                            'UserRol'=>array(
                                            'common_gestionUsuarios_getAll'=>array('ROLE_ADMIN'),
                                            'common_gestionRolUsuarios_getAll'=>array('ROLE_ADMIN')
                                            )
                            );

        $checkApproved = false;
        foreach ($entityApproved as $approved => $rutaApproved){
            if($checkApproved){continue;}
            if($approved == $entity){
                foreach ($rutaApproved as $ruta => $rolAssign){
                    if($route == $ruta){
                        foreach ($rolesUser as $rolUser){
                            foreach ($rolAssign as $rol){
                                if($rolUser == $rol){
                                    $checkApproved = true;    
                                }
                            }
                        }
                    }
                }
            }
        }

        return $checkApproved;
    }


##        #######   ######   
##       ##     ## ##    ##  
##       ##     ## ##        
##       ##     ## ##   #### 
##       ##     ## ##    ##  
##       ##     ## ##    ##  
########  #######   ######   
    private function logMessage($message, $error = false){
        if($error){
            $this->container->get('logger')->error($message);
        }else{
            $this->container->get('logger')->info($message);
        }
    }


}