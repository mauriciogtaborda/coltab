<?php

namespace CommonBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Debug\Debug;
Debug::enable();


class GETValidationController extends Controller
{

 ######   ######## ######## 
##    ##  ##          ##    
##        ##          ##    
##   #### ######      ##    
##    ##  ##          ##    
##    ##  ##          ##    
 ######   ########    ##    

    public function getDataAllAction(){
        try{
            $entity = $this->getRequest()->query->get('ent');
            $reference = $this->getRequest()->query->get('ref');
            $getData = $this->getRequest()->query->get('data');
            $getData2 = $this->getRequest()->query->get('data2');

            //SEGURIDAD 
            if(false === $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')){
                return new Response("{\"result\":\"Acceso Denegado\"}", 401);
                //throw new \Exception('Acceso Denegado');
            }

            //array('ENTIDAD','URL')
            $user = $this->container->get('security.context')->getToken()->getUser(); 
            $rolesUser = $user->getRoles();
            $rolUser = $rolesUser[0];
            $route = $this->getRequest()->attributes->get('_route');
            
            $RESTValidation = new RESTValidation();
            $checkApproved = $RESTValidation->entityANDurlApproved($entity, $route, $rolesUser);
            if(!$checkApproved){
                throw new \Exception("Entidad no Aprobada");
            }


            switch ($entity) {
                // case '':
                //     $getData = json_decode($getData);
                //     break;

            }

            //array('ENTIDAD','JMSgroup')
            $entityANDgroup = array(
                                    'Efector'=>'efector',
                                    'Carrera'=>'carrera',
                                    'Paciente'=>'paciente',
                                    'CalendarioComida'=>'calendarioComida',
                                    'User'=>'user',
                                    'UserRol'=>'userRol'
                                    );

            $checkGroup = false;
            foreach ($entityANDgroup as $approved => $JMSgroup){
                if($checkGroup){continue;}
                if($approved == $entity){
                    $checkGroup = true;
                    $group = $JMSgroup;
                }
            }

            //condicion default entidad
            //array('ENTIDAD','CLAUSULA')
            $entityCondition = array(
                                     'Paciente'=>array('fechaBaja'=>null),
                                     'Efector'=>array('fechaBaja'=>null),
                                     'Carrera'=>array('fechaBaja' =>null)
                                    );

            $checkCondition = false;
            foreach ($entityCondition as $approved => $condSymf){
                if($checkCondition){continue;}
                if($approved == $entity){
                    $checkCondition = true;
                    $condition = $condSymf;
                }
            }

            //array('ENTIDAD','REFERENCIA','CONDICION O CUSTOM QUERY')   
            $entityReference = array(
                                    'Efector' =>array(
                                                    'CalendarioComida'=>array('fechaBaja'=>null)
                                                    ),
                                     'Paciente'=>array(
                                                    'Racion'=>'query',
                                                    'Racion2'=>'query',
                                                    'UnidadInternacion'=>array('unidadInternacion'=>$getData),
                                                    'NotInfantes' => 'query',
                                                    'SoloInfantes' => 'query'
                                                    ),
                                     'CalendarioComida'=>array(
                                                    'Personal'=>array('personal'=>$getData)
                                                    )
                                     );

            $custonQuery = false;
            if($reference != ''){
                $checkCondition = false;
                if ($entity == 'Efector') {
                    foreach ($entityReference['Efector'] as $refEntity => $condSymf){
                        if($reference == $refEntity){
                            $checkCondition = true;
                            if ($efectorId && $reference != 'User') {
                                $condSymf['id'] = $efectorId;
                            }
                            $condition = $condSymf;
                        }
                    }
                }
                else
                    foreach ($entityReference as $approved => $refEnt){
                        if($checkCondition){continue;}
                        if($approved == $entity){
                            if($efectorId == ''){
                                foreach ($refEnt as $refEntity => $condSymf){
                                    if($reference == $refEntity){
                                        $checkCondition = true;
                                        $condition = $condSymf;
                                        if($condSymf == 'query'){
                                            $custonQuery = true;
                                            $flagRefEntity = $refEntity;
                                        }
                                    }  
                                }
                            }else{
                                foreach ($refEnt as $refEntity => $condSymf){
                                    if($reference == $refEntity){
                                        $checkCondition = true;
                                        if(isset($condSymf['efector'])){ 
                                            if($efectorId != $getData){
                                                return new Response("{\"result\":\"Acceso Denegado\"}", 401);            
                                            }
                                        }
                                        $condition = $condSymf;
                                        if($condSymf == 'query'){
                                            $custonQuery = true;
                                            $flagRefEntity = $refEntity;
                                        }
                                    }  
                                }
                            }
                        }
                    }
                if(!$checkCondition){
                    throw new \Exception('Informacion no encontrada');
                }
            }

            if($checkGroup){
                $em = $this->container->get('doctrine')->getManager();
                $qb = $em->createQueryBuilder();
                if($custonQuery){
                    $postDataResult = true;
                    switch ($flagRefEntity){
                     

                        // case 'ListadoAutorizadosdelDia':
                        //     $postDataResult = false;
                        //     $query = new APIListadosController();
                        //     $data = $query->getDataAction($em, $reference, $getData);
                            
                        //     break;
                    }
                    if($postDataResult){
                        $data = $query->getResult();
                    }
                }else{
                   if($checkCondition){
                        $data = $em->getRepository("CommonBundle:".$entity)->findBy($condition);
                 }else{
                        $data = $em->getRepository("CommonBundle:".$entity)->findAll();
                    }
                }

                if($this->container->getParameter('JSON_PRETTY_PRINT')){
                    $jsonContent = json_encode($data, JSON_PRETTY_PRINT);
                }else{
                    $serializer = SerializerBuilder::create()->build();
                    $jsonContent = $serializer->serialize($data, 'json', SerializationContext::create()->setGroups(array($group)));
                }
                //sleep(2);
                return new Response($jsonContent);
            }else{
                throw new \Exception('Informacion no encontrada');         
            }
        }catch (\Exception $e) {
            return new Response("{\"result\":\"".$e->getMessage()."\"}", 500);
        }
    }





##        #######   ######   
##       ##     ## ##    ##  
##       ##     ## ##        
##       ##     ## ##   #### 
##       ##     ## ##    ##  
##       ##     ## ##    ##  
########  #######   ######   
    private function logMessage($message, $error = false){
        if($error){
            $this->container->get('logger')->error($message);
        }else{
            $this->container->get('logger')->info($message);
        }
    }


}