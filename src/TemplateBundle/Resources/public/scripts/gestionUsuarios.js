viandaApp.controller('gestionUsuariosController', ['$scope', 'gestionRolUsuarios', 'gestionUsuarios', function($scope, gestionRolUsuarios, gestionUsuarios) {


  $scope.getData = function(){
    gestionRolUsuarios.query({entity:'UserRol'},function(roles){
    $scope.roles = roles;
      gestionUsuarios.query({entity:'User'},function(datasLists){
        angular.forEach(roles, function(valueRol, keyRol) {
          angular.forEach(datasLists, function(value, key) {
            if(value.roles[0] == valueRol.role){
               value['rol'] = valueRol.descripcion;
            }
          })
        })
        $scope.datasLists = datasLists;

        jQuery('#container_wait_modal').modal('hide');
      })  
    })
  }
  $scope.getData();

  $scope.add = function() {
    $scope.dataAdd = {};
    $scope.formAdd.$setPristine();
    $scope.formAdd.$setUntouched();
    jQuery('#addData .modal-header h3').html('Agregar Usuario');
    jQuery('#addData .modal-body').animate({scrollTop:(0)},500);
    jQuery('input[name=username]').prop('disabled',false);
    jQuery('select[name=role]').prop('disabled',false);
  }

  $scope.edit = function(data){
    $scope.dataAdd = angular.copy(data);
    jQuery('#addData .modal-header h3').html('Editar Usuario');
    jQuery('input[name=username]').prop('disabled',true);
    jQuery('select[name=role]').prop('disabled',true);
    jQuery('#addData .modal-body').animate({scrollTop:(0)},500);

  }
  $scope.del = function(data){
    $scope.dataDel = data;
  }

  $scope.saveData = function(data,form) {
    if(form.$valid){
      jQuery('#addData').modal('hide');
      jQuery('#container_wait_modal').modal('show');
      $scope.roles = '';
      gestionUsuarios.save({entity:'User'},data,
        function(){
          $scope.getData();        
        },
        function(jqXHR){
          errorMsg = jqXHR.data.result;
          jQuery('#container_wait_modal').modal('hide');
          jQuery('#warningMessege').modal('show');
          jQuery('#warningMessege .modal-body p').html(errorMsg);
        }
      )
    }
  }

  $scope.deleteData = function(data) {
    jQuery('#delData').modal('hide');
    jQuery('#container_wait_modal').modal('show');
    $scope.roles = '';
    gestionUsuarios.delete({entity:'User',data: data},
      function(){
        $scope.getData();      
      },
      function(jqXHR){
        errorMsg = jqXHR.data.result;
        jQuery('#container_wait_modal').modal('hide');
        jQuery('#warningMessege').modal('show');
        jQuery('#warningMessege .modal-body p').html(errorMsg);
      }
    )
  }

  $scope.blanqueoPass = function(data){
    $scope.dataAdd = data;
    jQuery('#blanqueoPass .modal-header h3').html('Blanqueo de Contraseña para '+data.username);
  }

  $scope.acceptBlanqueo = function(data){
      jQuery('#blanqueoPass').modal('hide');
      jQuery('#container_wait_modal').modal('show');
      $scope.roles = '';
      var blanqueo = {'blanqueo':true};
      data = angular.extend(data,blanqueo);
      gestionUsuarios.save({entity:'User'},data,
        function(jqXHR){
          message = jqXHR['result'];
          jQuery('#container_wait_modal').modal('hide');
          jQuery('#warningMessege').modal('show');
          jQuery('#warningMessege .modal-body p').html(message);
          jQuery('#warningMessege .modal-footer').removeClass('modal-danger');
          jQuery('#warningMessege .modal-footer').addClass('modal-success');        
        },
        function(jqXHR){
          errorMsg = jqXHR.data.result;
          jQuery('#container_wait_modal').modal('hide');
          jQuery('#warningMessege').modal('show');
          jQuery('#warningMessege .modal-body p').html(errorMsg);  
          jQuery('#warningMessege .modal-footer').removeClass('modal-success');        
          jQuery('#warningMessege .modal-footer').addClass('modal-danger');    
        })
  }

}])


