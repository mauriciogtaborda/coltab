var viandaApp = angular.module('panel-turnos', 
        ['ngRoute', 'ngResource', 'ngAnimate', 'ui.bootstrap', 'infinite-scroll'])       
        .config(function($interpolateProvider){
            $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
        })
        .config(['$httpProvider',function($httpProvider,$location) {
            $httpProvider.interceptors.push('myHttpInterceptor');
        }])
        .factory('myHttpInterceptor', function($q,$rootScope,$window) {
            // $rootScope.showSpinner = false;
            return {

              response: function(response) {
                //$rootScope.showSpinner = false;
                // do something on success
                //console.log('success');
                //console.log('status', response.status);
                //return response;
                return response || $q.when(response);
              },

             responseError: function(response) {
                if(response.status == 401){
                    $window.location.href = "/";
                }
                // do something on error
                //$rootScope.showSpinner = true;
                //console.log('failure');
                //console.log('status', response.status)
                //return response;
                return $q.reject(response);
              }
            };
          })
        .config(function($routeProvider){
            $routeProvider
                .when('/', {
                    templateUrl: 'inicio'
                })
                .when('/adminEfectores/', {
                    templateUrl: 'adminEfectores',
                    controller: 'adminEfectoresController'
                }) 
                 .when('/adminPacientes/', {
                    templateUrl: 'adminPacientes',
                    controller: 'adminPacientesController'
                })
                 .when('/gestionCalendarioComida/', {
                    templateUrl: 'gestionCalendarioComida',
                    controller: 'gestionCalendarioComidaController'
                })
                 .when('/gestionUsuarios/', {
                    templateUrl: 'gestionUsuarios',
                    controller: 'gestionUsuariosController'
                })  
                 .when('/adminCarreras/', {
                    templateUrl: 'adminCarreras',
                    controller: 'adminCarrerasController'
                })  
        })
        .run(function($rootScope, $location, $routeParams){
            $rootScope.$on('$routeChangeSuccess', function(newRoute, oldRoute){
  
                jQuery('#main-nav li').removeClass('active');
                jQuery('#container_wait_modal').modal('show');
                switch (oldRoute.loadedTemplateUrl){
                    case "inicio":
                        jQuery('.liInicio').addClass('active');
                        break;
                    case "adminEfectores":
                        jQuery('.liAdministrar').addClass('active');
                        break;       
                    case "adminPacientes":
                        jQuery('.liAdministrar').addClass('active');
                        break;
                    case "gestionCalendarioComida":
                        jQuery('.liGestion').addClass('active');
                        break;            
                    case "gestionUsuarios":
                        jQuery('.liUsuarios').addClass('active');
                        break;
                    case "adminCarreras":
                        jQuery('.liGestion').addClass('active');
                        break;
                }

            })

        })
        .factory('adminEfectores', function($resource){
            return $resource("api/adminEfectores?ent=:entity&ref=:reference");
        })
        .factory('adminPacientes', function($resource){
            return $resource("api/adminPacientes?ent=:entity&ref=:reference");
        })
        .factory('gestionCalendarioComida', function($resource){
            return $resource("api/gestionCalendarioComida?ent=:entity&ref=:reference");
        })
        .factory('gestionUsuarios', function($resource){
            return $resource("api/gestionUsuarios?ent=:entity&ref=:reference");
        })
        .factory('adminCarreras', function($resource){
            return $resource("api/adminCarreras?ent=:entity&ref=:reference");
        })