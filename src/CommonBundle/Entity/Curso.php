<?php

namespace CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation\Groups;

/**
 * Curso
 * 
 * @ORM\Table()
 * @ORM\Entity
 */
class Curso
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"curso"})
     */
    private $id;

    /**
     *
     * @ORM\Column(name="descripcion", type="string", length=127)
     * @Groups({"curso"})
     */
    private $descripcion;

    /**
     * @ORM\OneToMany(targetEntity="Proyecto", mappedBy="cursos")
     */
    protected $proyectos;


    /**
     * @ORM\ManyToOne(targetEntity="Carrera", inversedBy="cursos")
     * @ORM\JoinColumn(name="carrera_id", referencedColumnName="id")
     * @Groups({"servicio"})
     */
    protected $carreras;
    


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->proyectos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Curso
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Add proyectos
     *
     * @param \CommonBundle\Entity\Proyecto $proyectos
     * @return Curso
     */
    public function addProyecto(\CommonBundle\Entity\Proyecto $proyectos)
    {
        $this->proyectos[] = $proyectos;

        return $this;
    }

    /**
     * Remove proyectos
     *
     * @param \CommonBundle\Entity\Proyecto $proyectos
     */
    public function removeProyecto(\CommonBundle\Entity\Proyecto $proyectos)
    {
        $this->proyectos->removeElement($proyectos);
    }

    /**
     * Get proyectos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProyectos()
    {
        return $this->proyectos;
    }

    /**
     * Set carreras
     *
     * @param \CommonBundle\Entity\Carrera $carreras
     * @return Curso
     */
    public function setCarreras(\CommonBundle\Entity\Carrera $carreras = null)
    {
        $this->carreras = $carreras;

        return $this;
    }

    /**
     * Get carreras
     *
     * @return \CommonBundle\Entity\Carrera 
     */
    public function getCarreras()
    {
        return $this->carreras;
    }
}
