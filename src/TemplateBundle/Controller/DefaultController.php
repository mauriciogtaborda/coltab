<?php

namespace TemplateBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class DefaultController extends Controller
{
    public function authorizationChecker($template){
        $user = $this->container->get('security.context')->getToken()->getUser();        
        ($user != 'anon.') ? $roles = $user->getRoles() : $roles = array();

        $checkRoles = array(
                            'ROLE_ADMIN'=>array(
                                                'adminEfectoresAction',
                                                'adminPacientesAction',
                                                'gestionUsuariosAction',
                                                'adminCarrerasAction'
                                                ),
                            // 'ROLE_NUTRICIONISTA'=>array(
                            //                     'gestionCalendarioComidaAction',
                            //                     'gestionPersonalAutorizadoEventualAction',
                            //                     'gestionModuloEspecialAction',
                            //                     'gestionPersonalAutorizadoFijoAction',
                            //                     'gestionPersonalAutorizadoVariableAction',
                            //                     'gestionRacionDirectorAction',
                            //                     'gestionRegimenAlimentacionAction',
                            //                     'gestionModuloPacientesAction',
                            //                     'gestionControlRacionesAction',
                            //                     'gestionRegimenAlimentacionInfantesAction',
                            //                     'gestionMovimientosStockAction',
                            //                     'reportesDAction',
                            //                     'reportesAAction',
                            //                     'listadoDistribucionAction',
                            //                     'listadoRacionesEntregadasAutorizadosAction',
                            //                     'listadoAutorizadosdelDiaAction',
                            //                     'listadoLogSincronizacionAction'
                            //                     ),
                            // 'ROLE_JEFESERVICIO'=>array(
                            //                     'adminStockAction',
                            //                     'adminInsumosAction',
                            //                     'adminServiciosAction',
                            //                     'adminUnidadInternacionAction',
                            //                     'adminPersonalAction',
                            //                     'gestionCalendarioComidaAction',
                            //                     'gestionPersonalAutorizadoEventualAction',
                            //                     'gestionModuloEspecialAction',
                            //                     'gestionPersonalAutorizadoFijoAction',
                            //                     'gestionPersonalAutorizadoVariableAction',
                            //                     'gestionRacionDirectorAction',
                            //                     'gestionRegimenAlimentacionAction',
                            //                     'gestionModuloPacientesAction',
                            //                     'gestionControlRacionesAction',
                            //                     'gestionRegimenAlimentacionInfantesAction',
                            //                     'gestionMovimientosStockAction',
                            //                     'gestionUsuariosAction',
                            //                     'reportesDAction',
                            //                     'reportesAAction',
                            //                     'listadoDistribucionAction',
                            //                     'listadoRacionesEntregadasAutorizadosAction',
                            //                     'listadoAutorizadosdelDiaAction',
                            //                     'listadoLogSincronizacionAction'
                            //                     ),
                            // 'ROLE_REPORTES'=>array(
                            //                     'reportesDAction',
                            //                     'reportesAAction',
                            //                     'listadoDistribucionAction',
                            //                     'listadoRacionesEntregadasAutorizadosAction',
                            //                     'listadoAutorizadosdelDiaAction',
                            //                     'listadoLogSincronizacionAction'
                            //                     ),
                            // 'ROLE_JEFATURA'=>array(
                            //                     'gestionPersonalAutorizadoVariableAction'
                            //                     ),
                            // 'ROLE_HEMOTERAPIA'=>array(
                            //                     'gestionPersonalAutorizadoEventualAction'
                            //                     ),
                            // 'ROLE_CONCESIONARIO'=>array(
                            //                     'gestionControlRacionesAction'
                            //                     ),
                            // 'ROLE_RRHH'=>array(
                            //                     'adminPersonalAction'
                            //                     )
                            );
        
        $checkPermissions = false;
        foreach ($roles as $rolUser) {
            if($checkPermissions){continue;}
            foreach ($checkRoles as $role => $templateApprove) {
                if($rolUser == $role){
                    foreach ($templateApprove as $approve) {
                        if($approve == $template){
                            $checkPermissions = true;
                        }
                    }
                }    
            }
        } 

        if(!$checkPermissions){
            if(count($roles) != 0){
                throw new AccessDeniedException('Permiso Denegado.');
            }
        }
    }


    public function indexAction()
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        if($user != 'anon.'){
            $em = $this->container->get('doctrine')->getManager();
            $roles = $user->getRoles();
            $rol = $em->getRepository("CommonBundle:UserRol")->findOneByRole($roles[0]); 
            $arrayUser = array('roles'=>$user->getRoles(),
                               'rol'=>$rol->getDescripcion(),
                               'username'=>$user->getUsername(),
                               'nombre'=>$user->getNombre(),
                               'apellido'=>$user->getApellido(),
                               'docnro'=>$user->getDocnro()
                               );   
        }else{
            $arrayUser = array('roles'=>array(),
                               'rol'=>'',
                               'username'=>'',
                               'nombre'=>'',
                               'apellido'=>'',
                               'docnro'=>'',
                               );  
        }

        return $this->render('base.html.twig',$arrayUser);
    }

    public function inicioAction() {
        return $this->render('TemplateBundle:Default:inicio.html.twig');
    }

    public function adminEfectoresAction() {
        $this->authorizationChecker('adminEfectoresAction');
        return $this->render('TemplateBundle:Default:adminEfectores.html.twig');
    }

    public function gestionCalendarioComidaAction() {
        $this->authorizationChecker('gestionCalendarioComidaAction');
        return $this->render('TemplateBundle:Default:gestionCalendarioComida.html.twig');
    }

    public function adminPacientesAction() {
        $this->authorizationChecker('adminPacientesAction');
        return $this->render('TemplateBundle:Default:adminPacientes.html.twig');
    }

    public function adminCarrerasAction() {
        $this->authorizationChecker('adminCarrerasAction');
        return $this->render('TemplateBundle:Default:adminCarreras.html.twig');
    }

    public function gestionUsuariosAction() {
        $this->authorizationChecker('gestionUsuariosAction');
        return $this->render('TemplateBundle:Default:gestionUsuarios.html.twig');
    }


}
