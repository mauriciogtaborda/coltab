<?php
namespace CommonBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Console\Input\ArrayInput;


use CommonBundle\Entity\TipoDocumento;






class ViandasCargarTodosCommand extends ContainerAwareCommand {


    protected function configure() {
        $this
                ->setName('coltab:cargar:todos')
                ->setDescription('Cargar Datos Necesarios y Datos de prueba en la Base');
    }

    private function w($text){

        $this->output->writeln("<info>".$text."</info>");
        //$this->logger->info($text);
    }



    protected function execute(InputInterface $input, OutputInterface $output) {
        


            $command = $this->getApplication()->find('doctrine:fixture:load');

            $arguments = array(
                'command' => $command,
                //'--fixtures' => './src/CommonBundle/DataFixtures/ORM/Required',
            );

            $fixtureInput = new ArrayInput($arguments);
            $returnCode = $command->run($fixtureInput, $output);

            $this->container = $this->getApplication()->getKernel()->getContainer();
            $this->output = $output;

    }

}

