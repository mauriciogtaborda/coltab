<?php
namespace CommonBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use CommonBundle\Entity\UserRol;

class LoadRoles extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        $roles = array(
                       array("Administrador","ROLE_ADMIN"),
                       array("Profesor","ROLE_PROFESOR"),
                       array("Alumno","ROLE_ALUMNO")
                    );
                       
            foreach ($roles as $key => $value) {

                $userRol = new UserRol();
                $userRol->setDescripcion($value[0]);
                $userRol->setRole($value[1]);

                $manager->persist($userRol);
                $manager->flush();

            }

   	}

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 40; // the order in which fixtures will be loaded
    }


}