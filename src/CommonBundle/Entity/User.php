<?php
namespace CommonBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User extends BaseUser
{

    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"user"})
     */
    protected $id;

    /**
     * @var string
     * @Groups({"user"})
     */
    protected $username;

    /**
     * @var string
     * @Groups({"user"})
     */
    protected $roles;

    /**
     * @var string
     *
     * @ORM\Column(name="pin", type="string", length=10 , nullable = true)
     * @Groups({"user"})
     */
    private $pin;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=50 , nullable = false)
     * @Groups({"user"})
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellido", type="string", length=50 , nullable = false)
     * @Groups({"user"})
     */
    private $apellido;

    /**
     * @var string
     *
     * @ORM\Column(name="docnro", type="string", nullable = false , length=8) 
     * @Groups({"user"})
     */
    private $docnro;




    public function __construct()
    {
        parent::__construct();
        // your own logic
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return User
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellido
     *
     * @param string $apellido
     * @return User
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;

        return $this;
    }

    /**
     * Get apellido
     *
     * @return string 
     */
    public function getApellido()
    {
        return $this->apellido;
    }


    /**
     * Set docnro
     *
     * @param integer $docnro
     * @return User
     */
    public function setDocnro($docnro)
    {
        $this->docnro = $docnro;

        return $this;
    }

    /**
     * Get docnro
     *
     * @return integer 
     */
    public function getDocnro()
    {
        return $this->docnro;
    }

    /**
     * Set pin
     *
     * @param string $pin
     * @return User
     */
    public function setPin($pin)
    {
        $this->pin = $pin;

        return $this;
    }

    /**
     * Get pin
     *
     * @return string 
     */
    public function getPin()
    {
        return $this->pin;
    }
}
