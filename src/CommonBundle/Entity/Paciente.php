// <?php

// namespace CommonBundle\Entity;

// use Doctrine\ORM\Mapping as ORM;
// use Doctrine\Common\Collections\ArrayCollection;
// use JMS\Serializer\Annotation\Groups;

// /**
//  * Paciente
//  * 
//  * @ORM\Table()
//  * @ORM\Entity
//  */
// class Paciente
// {
//     /**
//      * @var integer
//      *
//      * @ORM\Column(name="id", type="integer")
//      * @ORM\Id
//      * @ORM\GeneratedValue(strategy="AUTO")
//      * @Groups({"paciente","racionModuloPaciente","moduloPacienteDetalle"})
//      */
//     private $id;

//     /**
//      * @var string
//      *
//      * @ORM\Column(name="nombre", type="string", length=127, nullable = false)
//      * @Groups({"paciente","racionModuloPaciente","moduloPacienteDetalle"})
//      */
//     private $nombre;

//     /**
//      * @var string
//      *
//      * @ORM\Column(name="apellido", type="string", length=127, nullable = false)
//      * @Groups({"paciente","racionModuloPaciente","moduloPacienteDetalle"})
//      */
//     private $apellido;

//     /**
//      * @ORM\ManyToOne(targetEntity="Genero", inversedBy="pacientes")
//      * @ORM\JoinColumn(name="genero_id", referencedColumnName="id", nullable=false)
//      * @Groups({"paciente"})
//      */
//     protected $genero;

//     /**
//      * @var string
//      *
//      * @ORM\Column(name="docnro", type="string", nullable = false)
//      * @Groups({"paciente","racionModuloPaciente"})
//      */
//     private $docnro;

//     /**
//      * @var string
//      *
//      * @ORM\Column(name="cama", type="string", nullable = true)
//      * @Groups({"paciente"})
//      */
//     private $cama;

//     /**
//      * @var \DateTime
//      *
//      * @ORM\Column(name="fechaNacimiento", type="date")
//      * @Groups({"paciente"})
//      */
//     private $fechaNacimiento;

//     /**
//      * @var \DateTime
//      *
//      * @ORM\Column(name="fechaBaja", type="datetime", nullable=true)
//      */
//     private $fechaBaja;

//     /**
//      * @var String
//      *
//      * @ORM\Column(name="observaciones", type="string", length=200, nullable = true)
//      * @Groups({"paciente"})
//      */
//     private $observaciones;

  
//     /**
//      * @ORM\ManyToOne(targetEntity="UnidadInternacion", inversedBy="pacientes")
//      * @ORM\JoinColumn(name="unidadinternacion_id", referencedColumnName="id", nullable= false)
//      * @Groups({"paciente"})
//      */
//     protected $unidadInternacion;

//     /**
//      * @ORM\ManyToOne(targetEntity="TipoDocumento", inversedBy="pacientes")
//      * @ORM\JoinColumn(name="tipodocumento_id", referencedColumnName="id", nullable= true)
//      * @Groups({"paciente"})
//      */
//     protected $tipoDocumento;


//     /**
//      * @ORM\OneToMany(targetEntity="Racion", mappedBy="paciente")
//      * @Groups({"paciente"})
//      */
//     protected $raciones;

//     /**
//      *
//      * @ORM\Column(name="isNotSigheos", type="boolean")
//      * @Groups({"paciente"})
//      */
//     private $isNotSigheos;



//     public function __construct()
//     {
//         $this->racionModulosPacientes = new ArrayCollection();
//     }


//     /**
//      * @ORM\OneToMany(targetEntity="RacionModuloPaciente", mappedBy="paciente")
//      */
//     protected $racionModuloPacientes;    




//     /**
//      * Get id
//      *
//      * @return integer 
//      */
//     public function getId()
//     {
//         return $this->id;
//     }

//     /**
//      * Set nombre
//      *
//      * @param string $nombre
//      * @return Paciente
//      */
//     public function setNombre($nombre)
//     {
//         $this->nombre = $nombre;

//         return $this;
//     }

//     /**
//      * Get nombre
//      *
//      * @return string 
//      */
//     public function getNombre()
//     {
//         return $this->nombre;
//     }

//     /**
//      * Set apellido
//      *
//      * @param string $apellido
//      * @return Paciente
//      */
//     public function setApellido($apellido)
//     {
//         $this->apellido = $apellido;

//         return $this;
//     }

//     /**
//      * Get apellido
//      *
//      * @return string 
//      */
//     public function getApellido()
//     {
//         return $this->apellido;
//     }


//     /**
//      * Set docnro
//      *
//      * @param string $docnro
//      * @return Paciente
//      */
//     public function setDocnro($docnro)
//     {
//         $this->docnro = $docnro;

//         return $this;
//     }

//     /**
//      * Get docnro
//      *
//      * @return string 
//      */
//     public function getDocnro()
//     {
//         return $this->docnro;
//     }

//     /**
//      * Set cama
//      *
//      * @param integer $cama
//      * @return Paciente
//      */
//     public function setCama($cama)
//     {
//         $this->cama = $cama;

//         return $this;
//     }

//     /**
//      * Get cama
//      *
//      * @return integer 
//      */
//     public function getCama()
//     {
//         return $this->cama;
//     }

//     /**
//      * Set fechaNacimiento
//      *
//      * @param \DateTime $fechaNacimiento
//      * @return Paciente
//      */
//     public function setFechaNacimiento($fechaNacimiento)
//     {
//         $this->fechaNacimiento = $fechaNacimiento;

//         return $this;
//     }

//     /**
//      * Get fechaNacimiento
//      *
//      * @return \DateTime 
//      */
//     public function getFechaNacimiento()
//     {
//         return $this->fechaNacimiento;
//     }

//     /**
//      * Set fechaBaja
//      *
//      * @param \DateTime $fechaBaja
//      * @return Paciente
//      */
//     public function setFechaBaja($fechaBaja)
//     {
//         $this->fechaBaja = $fechaBaja;

//         return $this;
//     }

//     /**
//      * Get fechaBaja
//      *
//      * @return \DateTime 
//      */
//     public function getFechaBaja()
//     {
//         return $this->fechaBaja;
//     }

//     /**
//      * Set observaciones
//      *
//      * @param string $observaciones
//      * @return Paciente
//      */
//     public function setObservaciones($observaciones)
//     {
//         $this->observaciones = $observaciones;

//         return $this;
//     }

//     /**
//      * Get observaciones
//      *
//      * @return string 
//      */
//     public function getObservaciones()
//     {
//         return $this->observaciones;
//     }

//     /**
//      * Set unidadInternacion
//      *
//      * @param \CommonBundle\Entity\UnidadInternacion $unidadInternacion
//      * @return Paciente
//      */
//     public function setUnidadInternacion(\CommonBundle\Entity\UnidadInternacion $unidadInternacion)
//     {
//         $this->unidadInternacion = $unidadInternacion;

//         return $this;
//     }

//     /**
//      * Get unidadInternacion
//      *
//      * @return \CommonBundle\Entity\UnidadInternacion 
//      */
//     public function getUnidadInternacion()
//     {
//         return $this->unidadInternacion;
//     }

//     /**
//      * Set tipoDocumento
//      *
//      * @param \CommonBundle\Entity\TipoDocumento $tipoDocumento
//      * @return Paciente
//      */
//     public function setTipoDocumento(\CommonBundle\Entity\TipoDocumento $tipoDocumento = null)
//     {
//         $this->tipoDocumento = $tipoDocumento;

//         return $this;
//     }

//     /**
//      * Get tipoDocumento
//      *
//      * @return \CommonBundle\Entity\TipoDocumento 
//      */
//     public function getTipoDocumento()
//     {
//         return $this->tipoDocumento;
//     }

//     /**
//      * Add raciones
//      *
//      * @param \CommonBundle\Entity\Racion $raciones
//      * @return Paciente
//      */
//     public function addRacione(\CommonBundle\Entity\Racion $raciones)
//     {
//         $this->raciones[] = $raciones;

//         return $this;
//     }

//     /**
//      * Remove raciones
//      *
//      * @param \CommonBundle\Entity\Racion $raciones
//      */
//     public function removeRacione(\CommonBundle\Entity\Racion $raciones)
//     {
//         $this->raciones->removeElement($raciones);
//     }

//     /**
//      * Get raciones
//      *
//      * @return \Doctrine\Common\Collections\Collection 
//      */
//     public function getRaciones()
//     {
//         return $this->raciones;
//     }

//     /**
//      * Add racionModulosPacientes
//      *
//      * @param \CommonBundle\Entity\RacionModuloPaciente $racionModulosPacientes
//      * @return Paciente
//      */
//     public function addRacionModulosPaciente(\CommonBundle\Entity\RacionModuloPaciente $racionModulosPacientes)
//     {
//         $this->racionModulosPacientes[] = $racionModulosPacientes;

//         return $this;
//     }

//     /**
//      * Remove racionModulosPacientes
//      *
//      * @param \CommonBundle\Entity\RacionModuloPaciente $racionModulosPacientes
//      */
//     public function removeRacionModulosPaciente(\CommonBundle\Entity\RacionModuloPaciente $racionModulosPacientes)
//     {
//         $this->racionModulosPacientes->removeElement($racionModulosPacientes);
//     }

//     /**
//      * Get racionModulosPacientes
//      *
//      * @return \Doctrine\Common\Collections\Collection 
//      */
//     public function getRacionModulosPacientes()
//     {
//         return $this->racionModulosPacientes;
//     }

//     /**
//      * Set genero
//      *
//      * @param \CommonBundle\Entity\Genero $genero
//      * @return Paciente
//      */
//     public function setGenero(\CommonBundle\Entity\Genero $genero = null)
//     {
//         $this->genero = $genero;

//         return $this;
//     }

//     /**
//      * Get genero
//      *
//      * @return \CommonBundle\Entity\Genero 
//      */
//     public function getGenero()
//     {
//         return $this->genero;
//     }

//     /**
//      * Add racionModuloPacientes
//      *
//      * @param \CommonBundle\Entity\RacionModuloPaciente $racionModuloPacientes
//      * @return Paciente
//      */
//     public function addRacionModuloPaciente(\CommonBundle\Entity\RacionModuloPaciente $racionModuloPacientes)
//     {
//         $this->racionModuloPacientes[] = $racionModuloPacientes;

//         return $this;
//     }

//     /**
//      * Remove racionModuloPacientes
//      *
//      * @param \CommonBundle\Entity\RacionModuloPaciente $racionModuloPacientes
//      */
//     public function removeRacionModuloPaciente(\CommonBundle\Entity\RacionModuloPaciente $racionModuloPacientes)
//     {
//         $this->racionModuloPacientes->removeElement($racionModuloPacientes);
//     }

//     /**
//      * Get racionModuloPacientes
//      *
//      * @return \Doctrine\Common\Collections\Collection 
//      */
//     public function getRacionModuloPacientes()
//     {
//         return $this->racionModuloPacientes;
//     }


//     /**
//      * Set isNotSigheos
//      *
//      * @param boolean $isNotSigheos
//      * @return Paciente
//      */
//     public function setIsNotSigheos($isNotSigheos)
//     {
//         $this->isNotSigheos = $isNotSigheos;

//         return $this;
//     }

//     /**
//      * Get isNotSigheos
//      *
//      * @return boolean 
//      */
//     public function getIsNotSigheos()
//     {
//         return $this->isNotSigheos;
//     }
// }
