var ColtabApp = angular.module('panel-coltab', 
        ['ngRoute', 'ngResource', 'ngAnimate', 'ui.bootstrap', 'infinite-scroll'])       
        .config(function($interpolateProvider){
            $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
        })
        .config(['$httpProvider',function($httpProvider,$location) {
            $httpProvider.interceptors.push('myHttpInterceptor');
        }])
        .factory('myHttpInterceptor', function($q,$rootScope,$window) {
            // $rootScope.showSpinner = false;
            return {

              response: function(response) {
                //$rootScope.showSpinner = false;
                // do something on success
                //console.log('success');
                //console.log('status', response.status);
                //return response;
                return response || $q.when(response);
              },

             responseError: function(response) {
                if(response.status == 401){
                    $window.location.href = "/";
                }
                // do something on error
                //$rootScope.showSpinner = true;
                //console.log('failure');
                //console.log('status', response.status)
                //return response;
                return $q.reject(response);
              }
            };
          })
        .config(function($routeProvider){
            $routeProvider
                .when('/', {
                    templateUrl: 'index'
                })
                 .when('/gestionUsuarios/', {
                    templateUrl: 'gestionUsuarios',
                    controller: 'gestionUsuariosController'
                })  
                .when('/admin/', {
                    templateUrl: 'admin',
                    controller: 'adminController'
                })  
        })
        .run(function($rootScope, $location, $routeParams){
            $rootScope.$on('$routeChangeSuccess', function(newRoute, oldRoute){
  
                jQuery('#main-nav li').removeClass('active');
                jQuery('#container_wait_modal').modal('show');
                switch (oldRoute.loadedTemplateUrl){
                    case "inicio":
                        jQuery('.liInicio').addClass('active');
                        break;
                    case "gestionUsuarios":
                        jQuery('.liUsuarios').addClass('active');
                        break;
                    
                }

            })

        })
        
        .factory('gestionUsuarios', function($resource){
            return $resource("api/gestionUsuarios?ent=:entity&ref=:reference");
        })
        .factory('admin', function($resource){
            return $resource("api/admin?ent=:entity&ref=:reference");
        })
        