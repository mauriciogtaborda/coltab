// <?php
// namespace CommonBundle\DataFixtures\ORM;

// use Doctrine\Common\DataFixtures\AbstractFixture;
// use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
// use Doctrine\Common\Persistence\ObjectManager;
// use CommonBundle\Entity\Paciente;

// class LoadPacientes extends AbstractFixture implements OrderedFixtureInterface
// {
//     /**
//      * {@inheritDoc}
//      */
//     public function load(ObjectManager $manager)
//     {

//         // Asegurar que se usan solo referencias creadas
//         $allrefs = array_keys($this->referenceRepository->getReferences());
//         $searchword = 'unidadInternacion';  
//         $this->unidadInternacionRefs = array_values(array_filter($allrefs, function($var) use ($searchword) { return preg_match("/$searchword/i", $var); }));

//        // Asegurar que se usan solo referencias creadas
//         $allrefs = array_keys($this->referenceRepository->getReferences());
//         $searchword = 'tipodoc';
//         $this->tipodocRefs = array_values(array_filter($allrefs, function($var) use ($searchword) { return preg_match("/$searchword/i", $var); }));

//         // Asegurar que se usan solo referencias creadas
//         $allrefs = array_keys($this->referenceRepository->getReferences());
//         $searchword = 'genero';
//         $this->generosRef = array_values(array_filter($allrefs, function($var) use ($searchword) { return preg_match("/$searchword/i", $var); }));


//         $boys = array("Santiago", "Mateo", "Sebastián", "Alejandro", "Matías", "Diego", "Samuel", "Nicolás", "Daniel", "Martín", "Benjamín", "Emiliano", "Leonardo", "Joaquín", "Lucas", "Iker", "Gabriel", "Thiago", "Adrián", "Bruno", "Dylan", "Tomás", "David", "Agustín", "Ian", "Ethan", "Felipe", "Maximiliano", "Eric", "Hugo", "Pablo", "Luca", "Rodrigo", "Ignacio", "Simón", "Carlos", "Javier", "Juan Pablo", "Isaac", "Santino", "Manuel", "Jerónimo", "Emmanuel", "Aarón", "Ángel", "Dante", "Gael", "Vicente", "Juan Sebastián", "Liam", "Damián", "Leo", "Francisco", "Alonso", "Christopher", "Álvaro", "Bautista", "Miguel Ángel", "Valentino", "Rafael", "Andrés", "Franco", "Fernando", "León", "Oliver", "Emilio", "Marcos", "Julián", "Juan José", "Pedro", "Alexander", "Lorenzo", "Mario", "Sergio", "Máximo", "Cristian", "Esteban", "Elías", "Antonio", "Luciano", "Noah", "Jorge", "Enzo", "Axel", "Salvador", "Marc", "Derek", "Juan Martín", "Joel", "Juan Diego", "Gonzalo", "Kevin", "Alan", "Eduardo", "Miguel", "Iván", "Josué", "Cristóbal", "Ciro", "Juan David");
//         $girls = array("Sofía", "Isabella", "Valentina", "Emma", "Camila", "Valeria", "Victoria", "Martina", "Ximena", "Luciana", "Lucía", "Sara", "Daniela", "Paula", "Julieta", "Renata", "Gabriela", "Elena", "Emilia", "Samantha", "María José", "Mía", "Natalia", "Mariana", "Antonella", "Regina", "Catalina", "Julia", "Andrea", "Amanda", "Zoe", "Olivia", "María", "Emily", "Carla", "Allison", "Alejandra", "Fernanda", "Abril", "Adriana", "Ana Sofía", "Guadalupe", "Miranda", "Agustina", "Noa", "Ariana", "Salomé", "Brianna", "Leirenuevo", "Alba", "Ainhoa", "Abigail", "Antonia", "Rafaela", "Nicole", "Ivanna", "Romina", "Ana", "Montserratnuevo", "Constanza", "Delfina", "Violeta", "Alma", "Laura", "Maia", "Florencia", "Claudia", "Amelia", "Juana", "Lola", "Guadalupe", "Alexa", "Irene", "Luna", "Elisa", "Ainaranuevo", "Francesca", "Maite", "Bianca", "Ashley", "Ariadna", "Alicianuevo", "Josefina", "Carmen", "María Paula", "Elizabeth", "Juliana", "Isabel", "Clara", "Eva", "Aitananuevo", "Mariangelnuevo", "Carolina", "Veranuevo", "Isidoranuevo", "Danna", "Ana Paula", "Matildanuevo", "Josefa", "Amalianuevo");
        
//         $apellidos = array("González","Rodríguez","Gómez","Fernández","López","Díaz","Martínez","Pérez","García","Sánchez","Romero","Sosa","Álvarez","Torres","Ruiz","Ramírez","Flores","Acosta","Benítez","Medina","Suárez","Herrera","Aguirre","Pereyra","Gutiérrez","Giménez","Molina","Silva","Castro","Rojas","Ortíz","Núñez","Luna","Juárez","Cabrera","Ríos","Ferreyra","Godoy","Morales","Domínguez","Moreno","Peralta","Vega","Carrizo","Quiroga","Castillo","Ledesma","Muñoz","Ojeda","Ponce","Vera","Vázquez","Villalba","Cardozo","Navarro","Ramos","Arias","Coronel","Córdoba","Figueroa","Correa","Cáceres","Vargas","Maldonado","Mansilla","Farías","Rivero","Paz","Miranda","Roldán","Méndez","Lucero","Cruz","Hernández","Agüero","Páez","Blanco","Mendoza","Barrios","Escobar","Ávila","Soria","Leiva","Acuña","Martin","Maidana","Moyano","Campos","Olivera","Duarte","Soto","Franco","Bravo","Valdéz","Toledo","Andrada-Andrade","Montenegro","Leguizamón","Chávez","Arce");

//         $pacientes = array();

//         foreach ($boys as $i => $boy) {

//             $Paciente = new Paciente();
//             $Paciente->setNombre($boy);
//             $Paciente->setApellido($apellidos[$i]);
//             //$Paciente->setSexo('M');
//             $Paciente->setGenero($this->getReference($this->generosRef[0]));
  

//             $pacientes[] = $this->setTheRest($Paciente);
//         }




//         shuffle($apellidos);

//         foreach ($girls as $i => $girl) {

//             $Paciente = new Paciente();
//             $Paciente->setNombre($girl);
//             $Paciente->setApellido($apellidos[$i]);
//             //$Paciente->setSexo('F');
//             $Paciente->setGenero($this->getReference($this->generosRef[1]));



//             $pacientes[] = $this->setTheRest($Paciente);
//         }


//         shuffle ($pacientes);
//         foreach ($pacientes as $key => $paciente) {

//             $this->addReference('paciente'. $key , $paciente);
//             $manager->persist($paciente);
//             $manager->flush();
//         }


//     }


//     public function setTheRest(Paciente $Paciente)
//     {


//             $Paciente->setCama(rand(1,99));
//             $doc = (string)rand(20,34) . sprintf("%06u", rand(0, 999999));
//             $Paciente->setDocNro((int)$doc);
//             $Paciente->setIsNotSigheos(true);
//             $Paciente->setUnidadInternacion($this->getReference($this->unidadInternacionRefs[rand(0, count($this->unidadInternacionRefs) - 1)]));
//             $Paciente->setTipoDocumento($this->getReference($this->tipodocRefs[rand(0, count($this->tipodocRefs) - 1)]));
//             $fecha = implode("-", array( (string)rand(1960,1998),(string)rand(1,12),(string)rand(1,28)));
//             $Paciente->setFechaNacimiento(date_create($fecha));
//             return $Paciente;
//     }

//     /**
//      * {@inheritDoc}
//      */
//     public function getOrder()
//     {
//         return 190; // the order in which fixtures will be loaded
//     }




// }
